![Daily Urinal](https://raw.githubusercontent.com/JohnHawley/daily-urinal/master/assets/img/urinal-logo.png "Logo")

Generates a daily newsletter to post above a urinal.

Uses the node package [x-ray](https://github.com/lapwinglabs/x-ray) to scrape website data and organizes it into a printable HTML document.

##Install

Requires node.js

Clone to your machine

Install dependencies: `npm install`

Run: `npm start` or `node start`

##Results
Results are outputted to a `print.html` file where the user can open and print from the browser.


##Config

Add production.json file to config directory

Run: `export NODE_ENV=production` before starting for first time to load production config.



#####Thanks

[@JohnHawly](https://twitter.com/johnhawly)

> I do not own the data scraped, I do not have permission to scrape this data. I'd rather use an API, however due to the quick nature of this fun little project, it was not in my best interest to find each API for each website and write adapters for each (and some sites do not offer this).  I'm only scraping these sites once a day, god have mercy ;)

#####TO-DO

Backend 
- Finish constructor thingys 
- Figure out this master app object thingys
- How to pass scrapper, or set the screpper per model
- How to pass each set of data to each of its respective views noce and clean like 

Frontend
- Page size
- Javascript drag and dropper
- Margin adjusts
- Input for joke
- Print button

- Maybe a sidebar module drag n' drop??