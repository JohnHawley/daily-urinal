// Exporting Public Functions
module.exports = {
    fancyDate: function () {
        // Get today
        var d = new Date();        

        // Today's Date
        var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        var today = monthNames[d.getMonth()] + " " + d.getUTCDate();
        var day = dayNames[d.getDay()];
        return `${day}, ${today}`;
    },
    yesterday: function () {
        // Get today
        var d = new Date();
        
        // Yesterday
        var yd = d;
        yd.setDate(yd.getDate() - 1);
        return yd;
    },
    weatherDayofWeek: function (date) {
        var dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        
        // For given date
        date = new Date(date);
        var dayName = dayNames[date.getDay()];

        // For today chack
        var today = new Date();
        var todayName = dayNames[today.getDay()];

        // For tomorrow check
        var tomorrow = new Date();
        tomorrow.setDate(today.getDate() + 1);
        var tomorrowName = dayNames[tomorrow.getDay()];

        switch (dayName) {
            case todayName:
                return "today";
                break;
            case tomorrowName:
                return "tomorrow";
                break;
            default:
                return dayName;
                break;
        }
    },
    isDay: function(days) {   
        // Setup like above
        const dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        var today = new Date();
        var todayName = dayNames[today.getDay()];
        todayName = todayName.toString().trim().toLowerCase();

        days = [].concat(days);

        ret = false; // Return value

        for (let i = 0; i < days.length; i++) {
            const element = days[i];
        
            days[i] = days[i].toString().trim().toLowerCase();

            if (days[i] == todayName) {
                ret = true;
            }
        }

        return ret;
    }
};