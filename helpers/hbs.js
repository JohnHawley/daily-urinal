// Exporting Public Functions
module.exports = {
    regHandlebarsHelpers: function (Handlebars) {

        // Condition operator // USE CASE: {{#ifCond var1 '==' var2}}
        Handlebars.registerHelper('ifCond', function (v1, operator, v2, options) {
            switch (operator) {
                case '==':
                    return (v1 == v2) ? options.fn(this) : options.inverse(this);
                case '===':
                    return (v1 === v2) ? options.fn(this) : options.inverse(this);
                case '!=':
                    return (v1 != v2) ? options.fn(this) : options.inverse(this);
                case '!==':
                    return (v1 !== v2) ? options.fn(this) : options.inverse(this);
                case '<':
                    return (v1 < v2) ? options.fn(this) : options.inverse(this);
                case '<=':
                    return (v1 <= v2) ? options.fn(this) : options.inverse(this);
                case '>':
                    return (v1 > v2) ? options.fn(this) : options.inverse(this);
                case '>=':
                    return (v1 >= v2) ? options.fn(this) : options.inverse(this);
                case '&&':
                    return (v1 && v2) ? options.fn(this) : options.inverse(this);
                case '||':
                    return (v1 || v2) ? options.fn(this) : options.inverse(this);
                default:
                    return options.inverse(this);
            }
        });

        // limit an array to a maximum of elements (from the start)
        Handlebars.registerHelper('limit', function (arr, limit) {
            return arr.slice(0, limit);
        });

        // If is only one
        Handlebars.registerHelper('ifsingle', function (arr, options) {
            'use strict';
            if (arr.length == 1) {
                return options.fn(this);
            }
            return options.inverse(this);
        });

        Handlebars.registerHelper("inc", function (value, options) {
            return parseInt(value) + 1;
        });

        Handlebars.registerHelper("filtTokens", function (value) {

            // Italics
            value = value.replace(/\{it\}/g, "<i>");
            value = value.replace(/\{\/it\}/g, "</i>");
            
            // Filter links
            value = value.replace(/\{d_link\|([^\|\}]*)([^\}]*)\}?/g, '$1')
            value = value.replace(/\{a_link\|([^\|\}]*)([^\}]*)\}?/g, '$1')
            value = value.replace(/\{et_link\|([^\|\}]*)([^\}]*)\}?/g, '$1')
                       
            // Filter anchor links
            value = value.replace(/<a\b[^>]*>(.*?)<\/a>/ig, '$1');

            // Remove
            value = value.replace(/[\x00-\x1F\x7F-\x9F]/g, '');
            value = value.replace(/\{bc\}/g, '');
            value = value.replace(/\{sx\|([^\|\}]*)([^\}]*)\}?/g, '$1');
            value = value.replace(/\{ma\}(.*)?\{\/ma\}/g, '');
            value = value.replace(/\{dx\}(.*)?\{\/dx\}/g, '');
            

            return value;
        });

    }
};