module.exports = async function (x) {
    async function fetchAsync() {
        const response = await x('https://api.reddit.com/r/Futurology/top.json?sort=top&t=day&limit=4');
        return await response.json();
    }

    var res = await fetchAsync();

    return res;
};