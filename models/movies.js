module.exports = async function (x) {

  scrape = await x("https://www.rottentomatoes.com/", {
    boxoffice: x("table#Top-Box-Office tr", [{
      icon: '.left_col span@class',
      score: '.tMeterScore',
      movie: 'td.middle_col',
      money: 'td.right_col',
    }]),
    opening: x("table#Opening tr", [{
      icon: '.left_col span@class',
      score: '.tMeterScore',
      movie: 'td.middle_col',
      money: 'td.right_col',
    }]),
    coming: x("table#Top-Coming-Soon tr", [{
      icon: '.left_col span@class',
      score: '.tMeterScore',
      movie: 'td.middle_col',
      money: 'td.right_col',
    }])
  });

  // Return the perfect object
  return scrape;

}