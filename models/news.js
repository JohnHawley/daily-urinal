const config = require('config');

module.exports = async function (x) {

  const key = config.get('News.key');

  const sources = [
    'associated-press',
    'reuters',
    'the-wall-street-journal',
    'abc-news'
  ];

  async function fetchAsync() {
      const api_url = 'https://newsapi.org/v2/top-headlines?sources='+sources.join(",")+'&sortBy=popularity&apiKey='+key;
      // console.log(api_url);
      const response = await x(api_url);
      return await response.json();
  }

  var res = await fetchAsync();

  return res;
}

// module.exports = async function (x) {
//   var scrape = await x("https://www.apnews.com/", 'div.main-story', {
//     headline: 'h1',
//     blurb: 'div.main-story-content div.main-story-text a.main-story-extract'
//   });

//   // Return the perfect object
//   return scrape;

// }
