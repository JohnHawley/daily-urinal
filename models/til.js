module.exports = async function (x) {

    async function fetchAsync() {
        const response = await x('https://api.reddit.com/r/todayilearned/top.json?sort=top&t=day&limit=10');
        return await response.json();
    }

    var res = await fetchAsync();

    return res;

}