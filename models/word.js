  // var ui = new inquirer.ui.BottomBar();
  var chalk = require('chalk');
  const axios = require('axios');
  const config = require('config');


  var Xray = require('x-ray');
  var NightmareElectron = require('x-ray-nightmare');
  var xNightmareDelay = Xray().driver(NightmareElectron(nightmareDriverDelay)); // < - - For JS/Browser Loaded content

  function nightmareDriverDelay(ctx, nightmare) {
      nightmare.goto(ctx.url)
          .wait(2000);
  }

  function allWordData(word, src) {

    const mwKey = config.get('Word.mw.key');
    const oxfordId = config.get('Word.oxford.id');
    const oxfordKey = config.get('Word.oxford.key');

      if (src == 'oxford') {
          // Oxford Dictionaries
          return axios.get('https://od-api.oxforddictionaries.com/api/v1/entries/en/' + word, {
              headers: {
                  'app_id': oxfordId,
                  'app_key': oxfordKey
              }
          });
      } else if (src == 'mw') {
          // Merriam-Webster
          return axios.get('https://www.dictionaryapi.com/api/v3/references/collegiate/json/' + word + '?key=' + mwKey);
      }
  }

  module.exports = async function (x, spin) {

      // For prompts
      var inquirer = require('inquirer');

      // Get the word of the day first
      var initWord = await x("https://www.merriam-webster.com/word-of-the-day", "main article", {
          word: 'div.wod-article-header div.word-and-pronunciation h1',
          simplePronounce: '.word-attributes span.word-syllables',
          didyouknow: 'div.did-you-know-wrapper div.left-content-box@html',
          example: 'div.wotd-examples@html',
      });

      // Stop spinner
      spin.stop(true);

      // Check with user if they want this word
      var ans = await inquirer.prompt([{
          type: 'input',
          name: 'word',
          message: 'Do you want to use this word?',
          default: initWord.word
      }]);

      // Start Spinner again
      spin.start();
      spin.text = chalk.green('%s ') + '[' + chalk.green('RESUMING') + '] With word: ' + chalk.cyan(ans.word);

    //   console.log(initWord.word);
    //   console.log(ans.word);
      if (initWord.word != ans.word) {

          // Get more data for the selected word
          let newWord = await x("https://www.merriam-webster.com/dictionary/" + ans.word, "definition-wrapper", {
              word: 'div.entry-header h1',
              simplePronounce: 'div.wod-article-header div.word-attributes span.word-syllables',
              didyouknow: '#note-1-anchor',
          });

          initWord = newWord;
      }


      // Begin word object
      let word = {};

      // Get more data for the selected word
      const src = 'mw';
      const wordData = await allWordData(ans.word, src);

      if (src == 'oxford') {
          // Oxford Dictionaries
          word.entries = wordData.data.results[0].lexicalEntries;
      } else if (src == 'mw') {
          // Merriam-Webster
          word.entries = wordData.data;
         
          word.simplePronounce = initWord.simplePronounce ? initWord.simplePronounce : false;
          word.history = initWord.didyouknow ? initWord.didyouknow : false;
          word.example = initWord.example ? initWord.example : false;
      }

      // Set the source for control object
      word.src = src;
      word.wordSearched = ans.word;

      // Scrape Etymology
      // word.etymology = await x("http://www.etymonline.com/index.php?allowed_in_frame=0&search=" + ans.word.replace(" ", "+"), '#dictionary', {
      //     etymology: 'dd.highlight'
      // });

      // ====== ETYMOLOGY ========= //
      // If marriam-websters Dictionary returns a "see [other word]" for etymology
      // Refetch that new word's etymology
      var testForSee = /\{dx\_ety\}([^\}]*)\{dxt\|([^\|\}]*)([^\}]*)\}?/g;
      if (word.entries) {
          if (Array.isArray(word.entries)) {
              word.entries = word.entries[0];
          }
          if (word.entries.et) {
              var seeDetected = testForSee.exec(word.entries.et[0][1]);
              //   console.log(seeDetected);
              if (seeDetected) {
                  if (typeof seeDetected[2] != null) {
                      const seeWord = await allWordData(seeDetected[2], src);
                      word.entries.et[0][1] = seeWord.data[0].et[0][1];
                  }
              }
          }
      }

      
      // Images from google
      word.imgs = await xNightmareDelay("https://www.google.com/search?q=google+dictionary#dobs=" + ans.word.replace(" ", "+"), '#rso', {
          origin: 'div:nth-child(1) > div div.dob-modules > div > div > div > div > div > div > div:nth-child(2) > g-img > img@src',
          use: 'div:nth-child(1) > div div.dob-modules > div > div > div > div > div:nth-child(7) > div > div > div:nth-child(2) > a > g-img > img@src'
      });
      // Return 'word' object
      return word;

  }