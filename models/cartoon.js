module.exports = async function (x) {
  var url = "http://dilbert.com/";
  var scrape = await x(url, '#js_comics', {
    url: '.comic-item img.img-comic@src'
  });
  // Return the perfect object
  return scrape;
}