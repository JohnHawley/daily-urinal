const config = require('config');

module.exports = async function (x) {

  const locationID = config.get('Weather.id');
  const apiKey = config.get('Weather.key');

  async function fetchAsync() {
      const response = await x('http://dataservice.accuweather.com/forecasts/v1/daily/5day/'+locationID+'?apikey='+apiKey+'&details=TRUE');
      return await response.json();
  }

  var res = await fetchAsync();

  return res;
};