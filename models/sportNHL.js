 // Get yesterdays date & format it specifically for this scrape
 var yd = require('../helpers/date.js').yesterday();
 yd = (yd.getFullYear()) + '' + ('0' + (yd.getMonth() + 1)).slice(-2) + '' + ('0' + yd.getDate()).slice(-2);

 module.exports = async function (x) {
     var url = "http://www.cbssports.com/nhl/scoreboard/" + yd + "/";
     var scrape = await x(url, 'div.score-card-container div.postgame', [{
         logo1: 'div.live-update > div.in-progress-table.section > table > tbody > tr:nth-child(1) > td.team > a > img@src',
         logo2: 'div.live-update > div.in-progress-table.section > table > tbody > tr:nth-child(2) > td.team > a > img@src',
         score1: 'div.live-update > div.in-progress-table.section > table > tbody > tr:nth-child(1) > td:last-of-type',
         score2: 'div.live-update > div.in-progress-table.section > table > tbody > tr:nth-child(2) > td:last-of-type',
         abrev1: 'div.live-update > div.in-progress-table.section > table > tbody > tr:nth-child(1) > td.team > a.team',
         abrev2: 'div.live-update > div.in-progress-table.section > table > tbody > tr:nth-child(2) > td.team > a.team',
         series: 'div.live-update > div.section.series-statement'
     }]);

     // Constructor
     for (var k in scrape) {
         var a1 = /nhl\/(.*)\.svg/g.exec(scrape[k].logo1);
         var a2 = /nhl\/(.*)\.svg/g.exec(scrape[k].logo2);
         scrape[k].abrev1 = a1[1];
         scrape[k].abrev2 = a2[1];

         scrape[k].series = scrape[k].series.trim();
         scrape[k].series = scrape[k].series.replace(': ', '<br>');

         if (Number(scrape[k].score1) > Number(scrape[k].score2)) {
             scrape[k].score1 = '<strong>' + scrape[k].score1 + '</strong>';
         } else {
             scrape[k].score2 = '<strong>' + scrape[k].score2 + '</strong>';
         }
     }

     // Return the perfect object
     return scrape;

 }