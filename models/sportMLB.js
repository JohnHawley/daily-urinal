  var fetch = require('node-fetch');
  var yd = require('./../helpers/date.js').yesterday();

  module.exports = async function (x, spin, team) {
    // console.log(yd.getDate()) - 1).slice(-2) + " ");

    // Url of data
    var url = "http://gd2.mlb.com/components/game/mlb/year_" + yd.getFullYear() + "/month_" + ('0' + (yd.getMonth() + 1)).slice(-2) + "/day_" + ('0' + (yd.getDate())).slice(-2) + "/master_scoreboard.json";

    // request
    var response = await fetch(url);

    // parsing
    var raw = await response.json();

    // Get the correct division standings
    switch (team) {
      case 'chc':
        division = 'C';
        league = 'NL';
        teamName = 'Cubs';
        teamID = 'chicago-cubs';
        break;
        case 'cws':
        division = 'C';
        league = 'AL';
        teamName = 'White Sox';
        teamID = 'chicago-white-sox';
        break;
      default:
        league = '';
        rowNum = '';
    }

    // Add team to raw object
    raw.team = team;
    raw.teamName = teamName;
    raw.division = division;
    raw.league = league;



    // cubs #regularSeason-division-205 > div > div > div.responsive-datatable__pinned > table

    // sox #regularSeason-division-202 > div > div > div.responsive-datatable__pinned > table
    // '#regularSeason-division-' + league + ' > div > div > div.responsive-datatable__pinned > table'



    // Scrape the correct division standings per team https://erikberg.com/mlb/standings.json

    // Url of data
    var url = "https://erikberg.com/mlb/standings.json";

    // request
    var response = await fetch(url, {
      headers: {
        'user-agent': 'DailyUrinalFetch/1.0 (jhawl42@gmail.com)'
      },
    });

    // parsing
    raw.standings = await response.json();
    raw.standings = raw.standings.standing;




    // raw.standings = await x('https://www.mlb.com/standings', 'html', [{
    //   body: 'main section@html',
    //   team: 'td:nth-child(1) a span.title-short',
    //   wins: 'td:nth-child(2)',
    //   loss: 'td:nth-child(3)',
    //   gb: 'td:nth-child(5)',
    //   lastTen: 'td:nth-child(7)'
    // }]);

    // console.log(raw.standings);

    // Return raw scrape
    return raw;

  }