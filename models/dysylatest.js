const config = require('config');

module.exports = async function (x) {

    const auth = config.get('DynamicSignal.auth');
    const user = config.get('DynamicSignal.user');
    const domain = config.get('DynamicSignal.domain');

    async function fetchAsync() {
        const response = await x('https://' + domain + '/v1/posts/categories?id=1', {
            headers: {
                "Authorization": "Bearer " + auth,
                "As-User": user
            }
        });
        return await response.json();
    }

    var res = await fetchAsync();

    return res;
}