module.exports = async function (x) {
    var url = "http://www.history.com/this-day-in-history/";
    var scrape = await x(url, '#lyra-wrapper > div.m-page-wrapper > div.m-advertisement-off-canvas--pusher > section > div.m-page > section.m-list-hub > section.m-card-group-container > section.m-card-group > div div.l-grid--item', [{
        // year: 'mm-card--tdih-year',
        year: 'div.mm-card--tdih-year',
        history: 'h2.m-card--header-text',
        excrept1: 'p.m-card--body',
    }]);
    return scrape;
}