/***
 * get live runtime value of an element's css style
 *   http://robertnyman.com/2006/04/24/get-the-rendered-style-of-an-element
 *     note: "styleName" is in CSS form (i.e. 'font-size', not 'fontSize').
 ***/
// var getStyle = function (e, styleName) {
//     var styleValue = "";
//     if(document.defaultView && document.defaultView.getComputedStyle) {
//         styleValue = document.defaultView.getComputedStyle(e, "").getPropertyValue(styleName);
//     }
//     else if(e.currentStyle) {
//         styleName = styleName.replace(/\-(\w)/g, function (strMatch, p1) {
//             return p1.toUpperCase();
//         });
//         styleValue = e.currentStyle[styleName];
//     }
//     return styleValue;
// }


jQuery(document).ready(function () {
    jQuery('.item').css('margin-bottom', 8); //default
    jQuery('.item').append("<aside class='toolbar'><button class='rm'><strong>X</strong></button></aside>");
    jQuery('.margin-adjust').append("<aside class='toolbar-margin'><button class='mar-up'><strong>&uarr;</strong></button>&nbsp;<button class='mar-down'><strong>&darr;</strong></button></aside>");
    jQuery('button.rm').on('click', function (ev) {
        ev.preventDefault();
        jQuery(this).parents().eq(1).remove();
    });
    jQuery('button.mar-up').on('click', function (ev) {
        ev.preventDefault();
        var col_items = jQuery(this).parents().eq(1).children('.item');
        var mar = col_items.css("margin-bottom");
        // do sumthin
        col_items.css('margin-bottom', (parseInt(mar, 10) - 1) + 'px');
    });
    jQuery('button.mar-down').on('click', function (ev) {
        ev.preventDefault();
        var col_items = jQuery(this).parents().eq(1).children('.item');
        var mar = col_items.css("margin-bottom");
        // do sumthin
        col_items.css('margin-bottom', (parseInt(mar, 10) + 1) + 'px');
    });
    jQuery("#logo-swap").click(function () {
        var img = $(this);
        if(img.attr('src') == 'assets/img/urinal-logo-short-2.jpg') {
            img.attr('src', 'assets/img/herinal-01.jpg');
        } else {
            img.attr('src', 'assets/img/urinal-logo-short-2.jpg');
        }
    });
});