module.exports = async function (raw) {
    var chalk = require('chalk');

    var numGames = raw.length;

    // Decide on bootstrap col num
    var col = 0;
    if ((numGames % 3) == 0) {
        col = 4;
    } else if (numGames == 1) {
        col = 12;
    } else if (numGames == 2) {
        col = 6;
    } else {
        col = 3;
    }

    // Get bootstrap col for frontend
    // raw.gameNum = col;

    // Loop to remove reddit self posts
    for (var j = 0; j < raw.length; j++) {
        raw[j].bs_col = col;
    }

    // Return the perfect nhlect
    return raw;

};