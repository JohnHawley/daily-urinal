module.exports = async function (til) {
    // Form list
    var rawlist = til.data.children;

    // Simplify data
    var list = rawlist.map(function (item) {
        return {
            content: item.data.title,
            src: item.data.domain,
            score: item.data.score
        }
    });

    // Filter by popularity
    var score = 10000;
    
    list = list.filter(function (item) {

        // Revert TIL string
        item.content = item.content.replace('TIL', '<strong>TIL</strong>');
        item.content = item.content.replace('TIL:', '<strong>TIL</strong>');
        item.content = item.content.replace('Today I Learned', '<strong>TIL</strong>');
        item.content = item.content.replace('Today I learned', '<strong>TIL</strong>');

        return item.score >= score;
    });

    return list;

};