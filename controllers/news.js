module.exports = async function (newslist) {

    const Xray = require('x-ray');
    const x = Xray(); // < - - For HTML

    const articleCount = 4;

    let finalNewsList = [];

    for (let i = 0; i < articleCount; i++) {
        const article = newslist.articles[i];
        let tempArticle = {};

        const blurb = (article.source.id == 'associated-press') ? await x(article.url, '#root > div > main > div.Body > div > div.Article > p:nth-child(1)') : article.description;

        tempArticle.headline = article.title;
        tempArticle.url = article.url;
        tempArticle.content =  await blurb;
        tempArticle.date = new Date(article.publishedAt).toLocaleDateString('en-US', { hour: 'numeric', minute: 'numeric', month: 'short', day: 'numeric' });
        tempArticle.source = article.source.name;

        finalNewsList.push(tempArticle);
    } 

    return finalNewsList;
};