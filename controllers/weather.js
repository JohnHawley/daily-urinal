module.exports = async function (res) {
    // require
    var d = require('./../helpers/date.js');

    // Form list
    var days = res.DailyForecasts;

    // Simplify data
    var days = days.map(function (item) {
        return {
            day: item.Date,
            temp: {
                hi: item.Temperature.Maximum.Value,
                lo: item.Temperature.Minimum.Value
            },
            desc: item.Day.LongPhrase,
        }
    });
    
    // Filter
    days = days.filter(function (item) {

        // Convert dates to day of week names
        item.day = d.weatherDayofWeek(item.day);

        return item;
    });

    return days;

};