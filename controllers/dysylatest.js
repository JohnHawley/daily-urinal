const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();

module.exports = async function (res) {

    posts = res.posts;

    // console.log(posts);

    var date = new Date(posts[0].publishDate);
    var options = { year: 'numeric', month: 'long', day: 'numeric' };

    // Just returned the latest post
    var post = {
        title: entities.encodeNonASCII(posts[0].title),
        description: (!posts[0].creatorComments) ? false : posts[0].description,
        comment: (posts[0].creatorComments != "") ? entities.encodeNonASCII(posts[0].creatorComments) : posts[0].description,
        author: posts[0].creatorInfo.displayName,
        date: date.toLocaleDateString("en-US", options)
    }

    // Filters
    post.description = (post.description == "Click to view the GIF") ? false : post.description;

    // Return the perfect object
    return post;

};