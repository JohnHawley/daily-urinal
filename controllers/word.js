var he = require('he');
const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();

module.exports = async function (raw) {

    var words = raw.entries;

    // console.log(words);

    switch (raw.src) {
        case 'oxford':
            var categories = [];

            for (var i = 0; i < words.length; i++) {

                var defs = [];

                //simplify senses
                for (let k = 0; k < words[i].entries[0].senses.length; k++) {
                    var sense = new Object();
                    if (!("definitions" in words[i].entries[0].senses[k])) {
                        return Promise.reject(new Error("No definition for this word?! Try: " + words[i].entries[0].senses[k].derivativeOf[0].text))
                    }
                    sense.definition = words[i].entries[0].senses[k].definitions[0];
                    sense.example = words[i].entries[0].senses[k].examples ? words[i].entries[0].senses[k].examples[0].text : false;
                    defs.push(sense);
                }

                var details = {
                    category: words[i].lexicalCategory,
                    definitions: defs
                }

                categories.push(details);
            }

            // Create word object
            var word = {
                word: words[0].text,
                pronounce: (words[0].pronunciations[0].phoneticSpelling) ? he.encode(words[0].pronunciations[0].phoneticSpelling) : false,
                etymology: (words[0].entries[0].etymologies) ? words[0].entries[0].etymologies[0] : false,
                categories: categories,
            };

            break;

        case 'mw':
            var categories = [];

            if (!Array.isArray(words)) {
                let ar = [];
                ar.push(words);
                words = ar;
            }

            for (var i = 0; i < words.length; i++) {

                var defs = [];
                //simplify senses

                const kindex = words[i].def[0].sseq;

                for (var k = 0; k < kindex.length; k++) {


                    for (let j = 0; j < words[i].def[0].sseq[k].length; j++) {

                        let sense = new Object();

                        const rawSense = words[i].def[0].sseq[k][j][1];

                        sense.num = rawSense.sn ? rawSense.sn : false;
                        sense.notSubsense = sense.num ? /([0-9]|[0-9]\ [a-z])/.test(sense.num) : true;

                        if(!rawSense.dt) {
                            if (sense.num) {
                                sense.definition = ""; // Just a place holder for the nums
                                defs.push(sense);
                            }
                            continue;
                        }

                        sense.definition = rawSense.dt ? rawSense.dt[0][1] : rawSense.sls[0];

                        if(Array.isArray(sense.definition))
                            continue;

                        if (rawSense.dt) {
                            sense.definition = rawSense.dt[0][1];
                            sense.example = rawSense.dt[1] ? rawSense.dt[1][1][0].t : false;
                        } else {
                            sense.definition = rawSense.sls[0];
                            sense.italics = true;
                        }
                        // console.log(sense.num);
                        
                        // senses.definition = words[i].def[0].sseq[k][0][1].dt[0][1];
                        // sense.shortdefinition = words[i].shortdef[j];
                        defs.push(sense);
                    }
                    // defs.push(senses);
                }
                // console.log(defs);

                var details = { // aka a "sense"
                    category: words[i].fl,
                    definitions: defs
                };
                // console.log(details);

                categories.push(details);
            }
                
            const firstword = (Array.isArray(words)) ? words[0] : words;
            
            // Create word object
            var word = {
                word: raw.wordSearched,
                pronounce: firstword.hwi.prs[0].mw ? entities.encodeNonASCII(firstword.hwi.prs[0].mw) : false,
                simplePronounce: raw.simplePronounce ? entities.encodeNonASCII(raw.simplePronounce) : false,
                etymology: firstword.et ? entities.encodeNonASCII(firstword.et[0][1]) : false,
                categories: categories,
                example: (raw.example) ? entities.encodeNonASCII(raw.example.replace(/\<h2\>Examples\<\/h2\>/g, '')) : 'No Example',
                didyouknow: entities.encodeNonASCII(raw.history)
            };

            // console.log(word);

            break;

        default:
            break;
    }



    if (raw.imgs.origin)
        word.origin = raw.imgs.origin;

    if (raw.imgs.use)
        word.use = raw.imgs.use;

    // console.log(word);

    // Return the perfect object
    return word;

};