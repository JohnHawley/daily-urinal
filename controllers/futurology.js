module.exports = async function (futurology) {

    // Form list
    const rawlist = futurology.data.children;

    // Simplify data
    let list = await Promise.all(rawlist.map(async (item) => {
        const iconFile = await getFuturologyIcon(item.data.link_flair_text);
        return {
            content: item.data.title,
            src: item.data.domain,
            score: item.data.score,
            flair: item.data.link_flair_text,
            icon: iconFile
        }
    }));

    // Filter by popularity
    const score = 3000;
    
    // Filter dosen't really work with asyc/await so I used this method:
    // https://stackoverflow.com/questions/47095019/how-to-use-array-prototype-filter-with-async
    const results = await Promise.all(list)
    const filtered_results = results.filter(item => {
        if (item.src != 'self.Futurology') {
            return (item.score >= score);
        }
    });

    // Return the perfect list
    return filtered_results;

};


async function getFuturologyIcon(flairText) {
    if (!flairText)
        return "default";

    // Load slugify
    const slugify = require('../helpers/slugify.js');

    flair = slugify(flairText);
    return flair;
}