module.exports = async function (rawMovies) {
    // require
    var movies = undefined;
    var isDay = await require('./../helpers/date.js').isDay;
    var title = 'Movies';


    if (isDay(['Monday', 'Tuesday'])) {
        title = "Weekend Box office";
        display = true;
        movies = rawMovies.boxoffice
    } else if (isDay(['Thursday', 'Friday'])) {
        title = "Movies opening soon";
        display = true;
        movies = rawMovies.opening.concat(rawMovies.coming);
    } else {
        return movies;
    }

    // Simplify data    
    var movies = movies.map(function (item) {
        return {
            icon: (item.icon) ? item.icon.trim() : '',
            score: (item.score) ? item.score.trim() : '',
            movie: item.movie.trim(),
            money: item.money.trim(),
        }
    });

    // Filter
    movies = movies.filter(async function (item) {

        // Get Icon Img
        item.icon = await getIconImage(item.icon);

        return item;
    });

    // Wait for all promises to fill
    movies = await Promise.all(movies);

    // Add display title
    movies.title = title;

    return movies;

};

async function getIconImage(raw) {

    // RE to get icon keyword
    var reIcon = /\s(\w+)$/;

    // Execute RE
    var res = reIcon.exec(raw);

    switch (res[1]) {
        case 'rotten':
            return "assets/img/movies-icons/rotten.png";
            break;
        case 'fresh':
            return "assets/img/movies-icons/fresh.png";
            break;
        case 'certified_fresh':
            return "assets/img/movies-icons/certified_fresh.png";
            break;
        default:
            return '';
            break;
    }
}