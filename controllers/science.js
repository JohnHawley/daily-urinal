module.exports = async function (science) {

    // Form list
    const rawlist = science.data.children;

    // Simplify data
    let list = await Promise.all(rawlist.map(async (item) => {
        const iconFile = await getScienceIcon(item.data.link_flair_text);
        return {
            content: item.data.title,
            src: item.data.domain,
            score: item.data.score,
            flair: item.data.link_flair_text,
            icon: iconFile
        }
    }));

    // Filter by popularity
    const score = 2500;

    // Filter dosen't really work with asyc/await so I used this method:
    // https://stackoverflow.com/questions/47095019/how-to-use-array-prototype-filter-with-async
    const results = await Promise.all(list)
    const filtered_results = results.filter(item => {
        if (item.src != 'self.science') {
            return (item.score >= score);
        }
    });

    // Return the perfect list
    return filtered_results;

};

async function getScienceIcon(flairText) {

    // Load slugify
    const slugify = require('../helpers/slugify.js');

    flair = slugify(flairText);
    return flair;
}