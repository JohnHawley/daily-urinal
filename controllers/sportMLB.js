module.exports = async function (raw) {
    // If there are any games
    if (raw.data.games.game) {
        var games = raw.data.games.game,
            team = raw.team,
            teamName = raw.teamName,
            standings = raw.standings,
            league = raw.league,
            division = raw.division,
            mlb = {};

        // Check if it's an array
        if (!(games instanceof Array)) {
            var a = [];
            a.push(games);
            games = a;
        }

        // Loop through all the games
        for (var i = 0; games.length > i; i++) {
            // Game find, then build out object
            if (games[i].away_file_code == team || games[i].home_file_code == team) {
                mlb.team = team;
                mlb.teamName = teamName;
                mlb.game = await parseMLB_Scores(games[i], team);
                // Find series or postseason record
                if (games[i].game_type == 'R') {
                    mlb.standings = await parseMLB_Standings(standings, teamName, division, league);
                } else if (games[i].game_type == 'S') {
                    mlb.standings = await parseMLB_Standings(standings, teamName, division, league);
                } else {
                    mlb.series = await parseMLB_Series(games[i]);
                }
            } else {
                // else did not play today and return a don't show command or something
            }
        }

        // Return the pretty MLB object
        return mlb;
    }
};

// Private function
async function parseMLB_Scores(game, team) {
    var result = {};

    result.awayTeamId = game.away_team_id;
    result.awayTeam = await filterAbrev(game.away_name_abbrev);
    result.awayScore = game.linescore.r.away;
    result.awayHits = game.linescore.h.away;
    result.awayErrors = game.linescore.e.away;
    result.awayLogo = await getLogo(result.awayTeam);
    result.awayWin = false;

    result.homeTeamId = game.home_team_id;
    result.homeTeam = await filterAbrev(game.home_name_abbrev);
    result.homeScore = game.linescore.r.home;
    result.homeHits = game.linescore.h.home;
    result.homeErrors = game.linescore.e.home;
    result.homeLogo = await getLogo(result.homeTeam);
    result.homeWin = false;

    // Well, who won?
    if (Number(result.homeScore) > Number(result.awayScore)) {
        result.homeWin = true;
    } else {
        result.awayWin = true;
    }

    return result;
}

// Private function
async function parseMLB_Standings(standings, team, division, league) {
    // Remove table headers 
    // standings.splice(0, 1);

    standings = standings.filter((theTeam) => {
        return ((theTeam.division == division) && (theTeam.conference == league));
    });

    standings = standings.map((theTeam) => {
        if (theTeam.last_name == team) {
            theTeam.featured = true;
            if (team == "White Sox")
                theTeam.last_name = "White&nbsp;Sox"
        }
        return theTeam;
    });

    
    // Return the perfect standings object
    return standings;
}


// Find series score
async function parseMLB_Series(game) {
    var series = {};

    series.description = game.description;
    series.venue = game.venue;

    if (game.home_win >= game.away_win) {
        series.score = game.home_win + ' - ' + game.away_win;
        series.teams = `${game.home_name_abbrev} - ${game.away_name_abbrev}`;
    } else {
        series.score = game.away_win + ' - ' + game.home_win;
        series.teams = `${game.away_name_abbrev} - ${game.home_name_abbrev}`;
    }

    return series;
}


async function filterAbrev(abrev) {
    switch (abrev) {
        case "CWS":
            abrev = "CHW";
            break;
        case "WSH":
            abrev = "WAS";
            break;
        default:
            break;
    }
    return abrev;
}

async function getLogo(team) {
    return `http://sportsfly.cbsistatic.com/bundles/sportsmediacss/images/team-logos/mlb/alt/${team}.svg`;
}