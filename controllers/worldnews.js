module.exports = async function (worldnews) {

    // Form list
    var rawlist = worldnews.data.children;

    // Simplify data
    var list = rawlist.map(function (item) {
        return {
            content: item.data.title,
            src: item.data.domain,
            score: item.data.score,
        }
    });

    // Filter by popularity
    var score = 15000;    
    list = list.filter(function(item) {
        return item.score >= score;
    });

    // Return the perfect list
    return list;
};