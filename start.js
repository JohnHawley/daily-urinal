// ---- Node config ---- //
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
//Added as a workaround for bad SSL Certs

// ---- Requires  ---- //
const fetch = require("node-fetch");
var NightmareElectron = require('x-ray-nightmare');
var Xray = require('x-ray');
var nightmareDriver = NightmareElectron(); // instantiate driver for later shutdown
var fs = require('fs-extra');
var exec = require('child_process').exec;
var Spinner = require('cli-spinner').Spinner;
var Handlebars = require('handlebars');
var chalk = require('chalk');
var fancyDate = require('./helpers/date.js').fancyDate();

// ---- Scraper Drivers ---- //
var x = Xray(); // < - - For HTML
var xNightmare = Xray().driver(nightmareDriver); // < - - For JS/Browser Loaded content

// Check if config files available
const config = require('config');
if (config.get('Word.mw.key') == "") {
  throw "Please make sure config files are not empty and run 'npm start'";
}


// Where to print the markup to?
var printFile = 'print.html';

// Configure Spinner
Spinner.setDefaultSpinnerString(18);
var spin = new Spinner({
  stream: process.stderr,
  onTick: function (msg) {
    this.clearLine(this.stream);
    this.stream.write(msg);
  }
});

// ---- Begin Application with unnessescary ASCII text logo ----- //
var fancyLogo = require('./helpers/gui.js').fancyLogo;
console.log(fancyLogo(fancyDate));

// ---- Trigger App (with empty app master obj) ---- //
asyncScrape(app = {});

// ---- Asynchronus functions called in order (Synchronusly) lolz ---- //
async function asyncScrape(app) {

  // Asynchronusly scrape each module and add it to the app master object
  // app.wordold = await get('wordold', xNightmareDelay);
  app.word = await get('word');
  // app.history = await get('history');
  app.dysylatest = await get('dysylatest', fetch);
  app.weather = await get('weather', fetch);
  app.til = await get('til', fetch);
  app.worldnews = await get('worldnews', fetch);
  app.futurology = await get('futurology', fetch);
  app.science = await get('science', fetch);
  app.cartoon = await get('cartoon', xNightmare);
  app.sportNBA = await get('sportNBA');
  // app.sportNHL = await get('sportNHL');
  app.sportMLB = [await get('sportMLB', x, 'chc'), await get('sportMLB', x, 'cws')]; // Using CBS Sports MLB codes
  app.news = await get('news', fetch);
  app.movies = await get('movies');

  // Build the Daily Urinal!
  build(app);

}


/*
 * Get Function, dynamically retrieving all the model data
 *
 * @param: name - The name of the content that needs to be fetched
 * @param:
 */
async function get(name, x = Xray(), opt) {

  // Start Loading Hourglass
  spin.start();

  // Loading Message
  spin.text = chalk.green('%s ') + '[' + chalk.green('GETTING') + '] ' + (name[0].toUpperCase() + name.slice(1)) + "...";

  // Find and set model
  var model = await require('./models/' + name + '.js')(x, spin, opt);

  // Set default for the returned result object -- This is needed for the OPTIONAL controller
  var result = model;

  // Check if controller file exists
  var file = './controllers/' + name + '.js';
  var fileExists = await fs.pathExists(file);
  if (fileExists) {
    // Find and execute controller and pass model
    result = await require('./controllers/' + name + '.js')(model);
  }

  // console.log('done');
  spin.stop(true)

  // Return Result object
  return result;
}


function build(app) {

  // --- Loading Message --- //
  spin.start();
  spin.text = chalk.green('%s ') + '[' + chalk.green('BUILDING') + '] The Daily Urinal!';

  app.fancyDate = fancyDate;

  // -- Register Handlebars Helpers -- //
  require('./helpers/hbs.js').regHandlebarsHelpers(Handlebars);

  // ======= Get all partials that are defined in dir ======================
  var partialsDir = __dirname + '/views/partials';
  var filenames = fs.readdirSync(partialsDir);

  filenames.forEach(function (filename) {
    var matches = /^([^.]+).hbs$/.exec(filename);
    if (!matches) {
      return;
    }
    var name = matches[1];
    var template = fs.readFileSync(partialsDir + '/' + filename, 'utf8');
    Handlebars.registerPartial(name, template);
  });
  // =======================================================================

  var source = fs.readFileSync('./views/body.hbs', 'utf8');
  var template = Handlebars.compile(source);
  var result = template(app);

  spin.stop(true);

  console.log('Finished building, Opening up HTML...');
  fs.writeFile(printFile, result, "ascii", (err) => {
    if (err) throw err;
    exec("open " + printFile);
    // exec("open https://www.reddit.com/r/Jokes/top/");
    console.log('[' + chalk.green("DONE") + '] ' + 'You may now stop this process.\n' + chalk.yellow('Enjoy your day! :)'));
  });

}